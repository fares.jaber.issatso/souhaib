import "./search.scss";

const SearchElement = ({ user, onClick }) => {
  return (
    <div className="search-element" onClick={() => onClick(user)}>
      <div className="card">
        <div className="row">
          <div className="left">{user.name}</div>
          <div className="right">{user.full_name}</div>
        </div>
        <div className="row">
          <div className="left">
            <div className="watchers-holder">
              <div className="icon">
                <img src={user.owner.avatar_url} alt={user.full_name} />
              </div>
              <div className="watchers text-bottom">
                <span>{user.watchers_count} watchers</span>
              </div>
            </div>
          </div>
          <div className="right text-bottom">
            <span>{user.language}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchElement;
