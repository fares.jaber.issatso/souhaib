import { useState } from "react";
import "./search.scss";
import data from "./data.json";
import SearchElement from "./SearchElement";

const SearchPage = () => {
  const [searchInputValue, setSearchInputValue] = useState("");
  const [searchInputFocus, setSearchInputFocus] = useState(false);
  const [searchData, setSearchData] = useState([]);
  const [fullViewElement, setFullViewElement] = useState();

  const performSearch = () => {
    setSearchData(data);
  };

  return (
    <div className="search">
      <div
        className={`input${
          searchInputFocus || searchInputValue ? " compact" : ""
        }`}
      >
        <input
          value={searchInputValue}
          onChange={(e) => setSearchInputValue(e.target.value)}
          onFocus={() => setSearchInputFocus(true)}
          onBlur={() => setSearchInputFocus(false)}
          placeholder="Search"
        />
        <button onClick={performSearch}>Chercher</button>
      </div>
      <div className="search-result">
        {!!fullViewElement ? (
          <SearchElement
            user={fullViewElement}
            onClick={() => setFullViewElement(null)}
          />
        ) : (
          searchData.map((element) => (
            <SearchElement
              key={element.id}
              user={element}
              onClick={setFullViewElement}
            />
          ))
        )}
      </div>
      <div className="repo-list">
        {fullViewElement && <div>afficher la liste des repo ici</div>}
      </div>
    </div>
  );
};

export default SearchPage;
